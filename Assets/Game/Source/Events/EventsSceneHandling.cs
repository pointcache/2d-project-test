﻿namespace Game.Events
{

    using EventBus;

    public struct OnBeforeSceneLoaded : IEvent
    {

    }

    public struct OnSceneSwitchReady : IEvent
    {

    }

    public struct OnAfterSceneLoaded : IEvent
    {

    }

    public struct RequestSceneSwitch : IEvent
    {
        public SceneRef.SceneReference scene;
    }

}