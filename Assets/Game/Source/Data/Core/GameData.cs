﻿namespace Game.Data
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class GameData : ScriptableObject
    {
        private static GameData instance;
        public static GameData Instance
        {
            get
            {
                if(instance == null)
                {
                    instance = Resources.Load<GameData>($"Game/Core/{nameof(GameData)}");
                }

                return instance;
            }
        }

        [SerializeField] private SceneData sceneData;
        public SceneData SceneData => sceneData;


    }

}