﻿namespace Game.Data
{
    using SceneRef;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class SceneData : ScriptableObject
    {
        [SerializeField] private SceneReference sceneMainMenu;
        public SceneReference SceneMainMenu => sceneMainMenu;

        [SerializeField] private SceneReference sceneGame;
        public SceneReference SceneGame => sceneGame;
    }

}