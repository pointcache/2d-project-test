﻿namespace Game.GUI
{
    using EventBus;
    using Game.Data;
    using Game.Events;
    using Game.Services.Core;
    using Game.Services.Scenes;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class GUIGame : MonoBehaviour
    {

        private void Awake()
        {
        }

        public void LoadMainMenu()
        {
            EventBus<RequestSceneSwitch>.Raise(new RequestSceneSwitch()
            {
                scene = GameData.Instance.SceneData.SceneMainMenu
            });
        }
    }

}