﻿namespace Game.Services.Scenes
{
    using Game.Services.Core;
    using SceneRef;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public abstract class SceneHandlerBase : MonoBehaviour, IService
    {


        public abstract void LoadScene(SceneReference sceneRef);
    }

}