﻿namespace Game.Services.Input
{
    using Game.Services.Core;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public abstract class PlayerInputBase : MonoBehaviour, IService
    {
        [SerializeField] protected string vertical = "Vertical";
        [SerializeField] protected string horizontal = "Horizontal";
        [SerializeField] protected string lookX = "Mouse X";
        [SerializeField] protected string lookY = "Mouse Y";
        [SerializeField] protected string attack = "Fire1";
        [SerializeField] protected string interact = "Fire2";

        protected InputState EmptyInputState => new InputState();
        public abstract InputState GetInput();

        protected KeyState resolveKeyState(KeyCode code)
        {
            if (Input.GetKeyDown(code))
                return KeyState.down;

            if (Input.GetKeyUp(code))
                return KeyState.up;

            if (Input.GetKey(code))
                return KeyState.pressed;

            return KeyState.none;
        }

        protected KeyState resolveKeyState(string key)
        {
            if (Input.GetButtonDown(key))
                return KeyState.down;

            if (Input.GetButtonUp(key))
                return KeyState.up;

            if (Input.GetButton(key))
                return KeyState.pressed;

            return KeyState.none;
        }
    }

}