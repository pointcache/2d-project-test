﻿using UnityEngine;

public enum KeyState
{
    none = 0,
    up = 1,
    down = 2,
    pressed = 3
}

public struct InputState
{
    public float vertical, horizontal;
    public bool hasMovementInput, hasLookInput;
    public float lookX, lookY;
    public Vector3 mousePosition, mouseWorldPosition, movementVector, lookVector;
    public KeyState attack, dash, run, interact;
    public KeyState weapon1, weapon2;

}
