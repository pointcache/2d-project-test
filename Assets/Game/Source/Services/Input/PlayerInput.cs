﻿namespace Game.Services.Input
{
    using Game.Services.Core;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;



    /// <summary>
    /// Provides input, and control over state (blocking, filtering)
    /// </summary>
    public class PlayerInput : PlayerInputBase
    {
        public bool PlayerInputActive { get; set; } = true;

        private long frame;
        private InputState lastInput;
        

        public override InputState GetInput()
        {
            if (!PlayerInputActive)
                return EmptyInputState;

            // Cache input once a frame, then return cache
            int currentFrame = Time.frameCount;
            if (currentFrame == frame)
                return lastInput;

            lastInput = new InputState()
            {
                vertical = Input.GetAxisRaw(vertical),
                horizontal = Input.GetAxisRaw(horizontal),
                lookX = Input.GetAxisRaw(lookX),
                lookY = Input.GetAxisRaw(lookY),
                mousePosition = Input.mousePosition,
                attack = resolveKeyState(attack),
                interact = resolveKeyState(interact),
                run = resolveKeyState(KeyCode.LeftShift),
                weapon1 = resolveKeyState(KeyCode.Alpha1),
                mouseWorldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition),
            };

            lastInput.hasMovementInput = lastInput.vertical != 0F || lastInput.horizontal != 0F;
            lastInput.hasLookInput = lastInput.lookX != 0F || lastInput.lookY != 0F;
            lastInput.movementVector = new Vector2(lastInput.horizontal, lastInput.vertical);
            lastInput.lookVector = new Vector2(lastInput.lookX, lastInput.lookY);

            frame = currentFrame;

            return lastInput;
        }

    }

}