﻿namespace Game.Services.Core
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class FallbackServices : ScriptableObject
    {
        public List<MonoBehaviour> servicePrefabs;
    }

}