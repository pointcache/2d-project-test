﻿namespace Game.Gameplay.Player
{
    using Game.Services.Core;
    using Game.Services.Input;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    [RequireComponent(typeof(Rigidbody2D))]
    public class PlayerMovementBase : MonoBehaviour
    {
        public float Speed { get; protected set; }

        [SerializeField] protected float speed = 2F;
        [SerializeField] protected float runSpeed = 5F;
        [SerializeField] protected float maxSpeed = 7F;
        [SerializeField] protected float deceleration = 5F;
    }

}