﻿namespace Game.Gameplay
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    [RequireComponent(typeof(SpriteRenderer))]
    public class InteractableObject : MonoBehaviour, IInteractable
    {
        [SerializeField] private SpriteRenderer spriteRenderer;

        public void OnInteraction()
        {
            spriteRenderer.color = Random.ColorHSV();
        }


    }

}