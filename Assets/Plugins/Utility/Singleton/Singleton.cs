﻿namespace Game.Utility.Singleton
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    [RequireComponent(typeof(SingletonInitializer))]
    public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T instance;
        public static T Instance => instance;

        protected virtual void Awake()
        {
            if (instance != null)
            {
                Debug.LogError($"Singleton {typeof(T).Name} already exists, fix it.");
            }

            instance = this as T;
        }
    } 
}