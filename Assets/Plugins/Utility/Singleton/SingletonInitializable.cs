﻿namespace Game.Utility.Singleton
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public interface ISingletonInitializable
    {
        void Initialize();
    }

    [RequireComponent(typeof(SingletonInitializer))]
    public class SingletonInitializable<T> : MonoBehaviour, ISingletonInitializable where T : MonoBehaviour
    {
        private static T instance;
        public static T Instance
        {
            get
            {
                if (instance == null)
                    instance = GameObject.FindObjectOfType<T>();

                return instance;
            }
        }

        void ISingletonInitializable.Initialize()
        {
            if (instance)
            {
                Debug.LogError($"Singleton::{typeof(T).Name}, was already registered, fix this.");
                enabled = false;
                return;
            }
            instance = this as T;
        }
    }

}