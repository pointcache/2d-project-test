﻿namespace Game.Utility.MonoFSM
{
    using System.Collections;
    using System.Collections.Generic;
    using DG.Tweening;
    using UnityEngine;
    using UnityEngine.Events;

    [RequireComponent(typeof(CanvasGroup))]
    public class MonoFSMStateTransitionable : MonoFSMStateBase
    {
        public bool SetActiveOnStateChange = true;
        public bool ForceExitStatesOnExit = true;
        
        [SerializeField]
        protected MonoFSM childFsm;

        [Header("Transition")]
        public bool DoTransition = true;
        public float TransitionDuration = 0.25F;
        public Ease AlphaFadeEase = Ease.InOutQuad;
        public Ease MovementEase = Ease.InOutQuad;

        public UnityEvent UOnStateEnter = new UnityEvent();
        public UnityEvent UOnStateExit = new UnityEvent();

        [SerializeField, HideInInspector]
        protected CanvasGroup canvasGroup;


        private void OnValidate()
        {
            if (!canvasGroup)
                canvasGroup = GetComponent<CanvasGroup>();

            if (!childFsm)
                childFsm = GetComponent<MonoFSM>();
        }

        protected override void OnStateEnterHandler(bool forced)
        {
            bool _doTransition = DoTransition;
            if (forced)
                _doTransition = false;

            if(childFsm && ForceExitStatesOnExit)
            {
                childFsm.RestoreLastState();
            }

            if (SetActiveOnStateChange)
            {
                gameObject.SetActive(true);
            }
            if (_doTransition)
            {
                canvasGroup.DOKill();
                canvasGroup.DOFade(1f, TransitionDuration)
                    .SetEase(AlphaFadeEase)
                    .OnComplete(StateEnterTransitionCompleteHandler);

                CurrentStatus = Status.transitioning;
            }
            else
            {
                StateEnterTransitionCompleteHandler();
            }
        }

        protected virtual void StateEnterTransitionCompleteHandler()
        {
            canvasGroup.alpha = 1f;
            canvasGroup.interactable = true;
            canvasGroup.blocksRaycasts = true;
            CurrentStatus = Status.active;
        }

        protected override void OnStateExitHandler(bool forced)
        {
            bool _doTransition = DoTransition;
            if (forced)
                _doTransition = false;

            canvasGroup.interactable = false;
            canvasGroup.blocksRaycasts = false;
            canvasGroup.DOKill();

            if (_doTransition)
            {
                canvasGroup.DOFade(0f, TransitionDuration)
                    .SetEase(AlphaFadeEase)
                    .OnComplete(StateExitTransitionCompleteHandler);

                CurrentStatus = Status.transitioning;
            }
            else
            {
                StateExitTransitionCompleteHandler();
            }
        }

        protected virtual void StateExitTransitionCompleteHandler()
        {
            CurrentStatus = Status.inactive;
            canvasGroup.alpha = 0f;
            if (ForceExitStatesOnExit && childFsm)
            {
                childFsm.ForceExitAllStates();
            }
            if (SetActiveOnStateChange)
            {
                gameObject.SetActive(false);
            }
            
        }
    }
}