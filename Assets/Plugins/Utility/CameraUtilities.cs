﻿namespace Game.Utility
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public static class CameraUtilities 
    {
        public static Vector3 MouseWorldPos(this Camera cam, float Z)
        {
            var mouse = Input.mousePosition;
            mouse.z = Z;
            return cam.ScreenToWorldPoint(mouse);
        }


    }

}