﻿namespace Game.Utility
{

    using UnityEngine;
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Reflection.Emit;
    using System.Linq;
    using System.Collections;
    using System.IO;
#if UNITY_EDITOR
    using UnityEditor;
#endif


    public class Enumerators
    {

        public static IEnumerator Addframedelay(Action action)
        {
            yield return new WaitForEndOfFrame();
            action();
        }
    }


    public static class GameObjectUtils
    {

        public static void RunOnEachOfType<T>(this GameObject go, Action<T> action) where T : Component
        {

            var comp = go.GetComponent<T>();
            if (((object)comp) != null)
                action(comp);

            Transform tr = go.transform;
            int childcount = tr.childCount;

            if (childcount > 0)
            {

                for (int i = 0; i < childcount; i++)
                {

                    _runOnEachOfType<T>(tr.GetChild(i), action);

                }
            }
        }

        private static void _runOnEachOfType<T>(Transform tr, Action<T> action) where T : Component
        {

            int childcount = tr.childCount;

            if (childcount > 0)
            {

                for (int i = 0; i < childcount; i++)
                {

                    _runOnEachOfType<T>(tr.GetChild(i), action);

                }
            }

            var comp = tr.gameObject.GetComponent<T>();
            if (((object)comp) != null)
                action(comp);

        }

#if UNITY_EDITOR
        public static T FindScriptableObject<T>() where T : ScriptableObject
        {
            T so = null;
            var assets = AssetDatabase.FindAssets("t:" + typeof(T).Name);
            if (assets.Length == 0)
            {
                Debug.LogError(typeof(T).Name + " file was not found. (If you see this message on project import, and the scriptable object exists, it means it was not yet imported, rerun parsers and updaters.)");
            }
            else
            {
                so = AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(assets[0]), typeof(T)) as T;
            }
            return so;
        }
#endif
        public static Transform[] GetChildrenWithTag(this GameObject go, string tag)
        {
            return go.transform.Cast<Transform>().Where(c => c.gameObject.CompareTag(tag)).ToArray();
        }

        public static Transform[] GetChildrenWithTag(this Transform tr, string tag)
        {
            return tr.Cast<Transform>().Where(c => c.gameObject.CompareTag(tag)).ToArray();
        }

        public static bool DisableIfNotFound(this MonoBehaviour mono, object obj, string errorMessage)
        {
            if (obj == null)
            {
                Debug.LogError(errorMessage, mono);
                mono.gameObject.SetActive(false);
                return true;
            }
            else
                return false;
        }

        public static void SetLayerRecursive(this GameObject go, int layer)
        {
            go.layer = layer;
            setlayerrecursive(go.transform, layer);
        }

        static void setlayerrecursive(Transform tr, int layer)
        {
            foreach (Transform child in tr)
            {
                setlayerrecursive(child, layer);
                child.gameObject.layer = layer;
            }
        }


        public static void AddComponentToAllChildren<T>(this GameObject mono, bool removeAllIfExists) where T : Component
        {
            add_component_recursive<T>(mono.transform, removeAllIfExists);
        }

        public static void AddComponentToAllChildren<T>(this GameObject mono) where T : Component
        {
            add_component_recursive<T>(mono.transform, false);
        }

        static void add_component_recursive<T>(Transform tr, bool removeIfExists) where T : Component
        {
            foreach (Transform c in tr)
            {
                add_component_recursive<T>(c, removeIfExists);
                if (removeIfExists)
                {
                    var comps = c.gameObject.GetComponents<T>();
                    foreach (var co in comps)
                    {
#if UNITY_EDITOR
                        GameObject.DestroyImmediate(co);
#endif
                    }
                }
                c.gameObject.AddComponent<T>();
            }
        }

        public static void OneFrameDelay(this MonoBehaviour mb, Action action)
        {
            mb.StartCoroutine(Enumerators.Addframedelay(action));
        }
        public static GameObject Spawn(string path)
        {
            return Spawn(path, false);
        }

        public static GameObject Spawn(string path, bool startDisabled, Vector3 position, Quaternion rotation)
        {
            bool initstate;
            var pref = Resources.Load(path) as GameObject;
            if (pref == null)
            {
                Debug.LogError("Did not find prefab by path: " + path);
                return null;
            }
            initstate = pref.activeSelf;
            if (startDisabled)
                pref.gameObject.SetActive(false);
            if (pref)
            {
                GameObject go = GameObject.Instantiate(pref, position, rotation);
                pref.SetActive(initstate);
                return go;
            }
            else
            {
                Debug.LogError("Resource:" + path + " not found.");
                return null;
            }
        }

        public static GameObject Spawn(string path, bool startDisabled)
        {
            return Spawn(path, startDisabled, new Vector3(0, 0, 0), Quaternion.identity);
        }

        public static GameObject Spawn(GameObject prefab, bool startDisabled)
        {
            if (prefab == null)
                return null;

            bool initstate;
            initstate = prefab.activeSelf;

            try
            {
                if (startDisabled)
                    prefab.SetActive(false);
                if (prefab)
                {
                    GameObject go = GameObject.Instantiate(prefab);
                    prefab.SetActive(initstate);
                    return go;
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e);
                prefab.SetActive(initstate);
            }

            return null;
        }

#if UNITY_EDITOR
        public static GameObject SpawnEditor(GameObject pref)
        {
            if (pref)
            {
                var go = PrefabUtility.InstantiatePrefab(pref) as GameObject;
                Selection.activeGameObject = go;
                //SceneView.lastActiveSceneView.FrameSelected();
                return go;
            }
            else
                return null;
        }

        public static GameObject SpawnEditor(string path)
        {
            var pref = Resources.Load(path) as GameObject;
            if (pref)
            {
                var go = PrefabUtility.InstantiatePrefab(pref) as GameObject;
                Selection.activeGameObject = go;
                SceneView.lastActiveSceneView.FrameSelected();
                return go;
            }
            else
                return null;
        }

#endif
                          
    }
}