﻿namespace Game.Utility
{
    using UnityEngine;
    using System;
    using System.Collections.Generic;

    public static class TransformUtilities
    {

        public static Vector3 GetAxis(this Axis axis)
        {
            switch (axis)
            {
                case Axis.x:
                return Vector3.right;

                case Axis.y:
                return Vector3.up;

                case Axis.z:
                return Vector3.forward;
            }
            return Vector3.zero;
        }

        public static List<Transform> GetTransformsInOrder(this GameObject go)
        {
            var tr = go.transform;
            List<Transform> list = new List<Transform>(tr.childCount);
            foreach (Transform t in tr)
            {
                list.Add(t);
            }

            return list;
        }

        public static void SetX(this Transform tr, float x)
        {
            Vector3 pos = tr.position;
            pos.x = x;
            tr.position = pos;
        }

        public static void SetY(this Transform tr, float y)
        {
            Vector3 pos = tr.position;
            pos.y = y;
            tr.position = pos;
        }

        public static void SetZ(this Transform tr, float z)
        {
            Vector3 pos = tr.position;
            pos.z = z;
            tr.position = pos;
        }

        public static void GetChildren(this Transform tr, List<Transform> container)
        {
            if (container == null)
            {
                Debug.LogError("List is null");
                return;
            }
            foreach (Transform child in tr)
            {
                container.Add(child);
            }
        }

        public static void DestroyChildren(this Transform tr)
        {
            int count = tr.childCount;
            if (count == 0)
                return;
            

            for (int i = count-1; i >= 0; i--)
            {
#if UNITY_EDITOR
                if (!Application.isPlaying)
                    GameObject.DestroyImmediate(tr.GetChild(i).gameObject);
                else
                    GameObject.Destroy(tr.GetChild(i).gameObject);
#else
                    GameObject.Destroy(tr.GetChild(i).gameObject);
#endif
            }
        }

        private static List<Transform> localBuffer = new List<Transform>();
        public static void GetComponentsInChildrenOnly<T>(this GameObject go, ref List<T> buffer) where T : Component
        {
            if (buffer == null)
            {
                buffer = new List<T>();
            }
            else
            {
                buffer.Clear();
            }


            localBuffer.Clear();
            go.transform.GetAllChildren(ref localBuffer);
            int count = localBuffer.Count;
            for (int i = 0; i < count; i++)
            {
                T c = localBuffer[i].GetComponent<T>();
                if (c != null)
                {
                    buffer.Add(c);
                }
            }
        }

        public static void GetAllChildren(this Transform tr, ref List<Transform> buffer)
        {
            if (buffer == null)
            {
                buffer = new List<Transform>();
            }
            _GetAllChildrenRecursive(tr, buffer);
        }

        private static void _GetAllChildrenRecursive(Transform tr, List<Transform> buffer)
        {
            foreach (Transform child in tr)
            {
                buffer.Add(child);
                _GetAllChildrenRecursive(child, buffer);
            }
        }

        public static void SetAllChildren(this Transform tr, bool state)
        {
            foreach (Transform t in tr)
            {
                t.gameObject.SetActive(state);
            }
        }

        public static Vector2 GetRealMax(this RectTransform rect)
        {
            float x = rect.position.x + (rect.sizeDelta.x * (1 - rect.pivot.x));
            float y = rect.position.y + (rect.sizeDelta.y * (1 - rect.pivot.y));
            return new Vector2(x, y);
        }

        public static Vector2 GetRealMin(this RectTransform rect)
        {
            float x = (rect.position.x - (rect.sizeDelta.x * rect.pivot.x));
            float y = rect.position.y - (rect.sizeDelta.y * rect.pivot.y);
            return new Vector2(x, y);
        }

        public static void DisableAllChildren(this Transform tr)
        {
            foreach (Transform t in tr)
            {
                t.gameObject.SetActive(false);
            }
        }

        //Breadth-first search
        public static Transform FindDeepChild(this Transform aParent, string aName)
        {
            var result = aParent.Find(aName);
            if (result != null)
                return result;
            foreach (Transform child in aParent)
            {
                result = child.FindDeepChild(aName);
                if (result != null)
                    return result;
            }
            return null;
        }

        /// <summary>
        /// Deep search, returns first found
        /// </summary>
        /// <param name="transform"></param>
        /// <param name="Tag"></param>
        /// <returns></returns>
        public static Transform FindChildWithTag(this Transform transform, string Tag)
        {

            Transform result = null;

            foreach (Transform tr in transform)
            {
                if (tr.tag == Tag)
                    result = tr;
            }

            if (result != null)
                return result;

            foreach (Transform child in transform)
            {
                result = child.FindChildWithTag(Tag);
                if (result != null)
                    return result;
            }

            return null;
        }

        public static void ZeroOut(this Transform tr)
        {
            tr.position = Vector3.zero;
            tr.rotation = Quaternion.identity;
        }

        public static void ZeroOutLocal(this Transform tr)
        {
            tr.localPosition = Vector3.zero;
            tr.localRotation = Quaternion.identity;
        }

        public static Vector2[] GetScreenCorners(this RectTransform tr)
        {
            Vector3[] corners = new Vector3[4];
            tr.GetWorldCorners(corners);
            Vector2[] s_corners = new Vector2[4];
            for (int i = 0; i < 4; i++)
            {
                s_corners[i].x = corners[i].x;
                s_corners[i].y = corners[i].y;
            }

            return s_corners;
        }

        public static float DistanceTo(this Transform transform, Transform other)
        {
            return Vector3.Distance(transform.position, other.position);
        }

        public static float DistanceTo(this Transform transform, Vector3 other)
        {
            return Vector3.Distance(transform.position, other);
        }
    }

}