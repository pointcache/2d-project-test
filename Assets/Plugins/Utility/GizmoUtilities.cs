﻿namespace Game.Utility
{
#if UNITY_EDITOR
    using UnityEngine;
    using System.Collections;
    using UnityEditor;

    public static class GizmoUtilities
    {


        public static class BoundsDrawer
        {
            public static void DrawGameObjectBounds(GameObject go)
            {
                var c = go.GetComponentInChildren<Collider>();
                if (c)
                {
                    DrawBounds(c.bounds);
                    return;
                }

                var mr = go.GetComponentInChildren<MeshRenderer>();
                if (mr)
                {
                    DrawBounds(mr.bounds);
                    return;
                }

                DrawBounds(new Bounds(go.transform.position, Vector3.one));
            }

            public static void DrawBounds(Bounds bounds)
            {
                float boundsShell = 0.1F;
                bounds.Expand(boundsShell);

                float maxCornerLength = 5F;
                float minCornerLength = 0.05F;
                float divisor = 8F;
                float sizeX = bounds.size.x;
                float sizeY = bounds.size.y;
                float sizeZ = bounds.size.z;

                float cornerX = Mathf.Clamp(sizeX / divisor, minCornerLength, maxCornerLength);
                float cornerY = Mathf.Clamp(sizeY / divisor, minCornerLength, maxCornerLength);
                float cornerZ = Mathf.Clamp(sizeZ / divisor, minCornerLength, maxCornerLength);
                Handles.color = new Color32(255, 255, 255, 180);

                /* ^ z
                   | 
                     ---> x

                   B -------- C
                   |          |
                   |          |
                   |          |
                   |          |
                   A--------- D
                */

                Vector3 boundsMin = bounds.min;
                Vector3 boundsMax = bounds.max;

                float maxY = boundsMax.y;

                Vector3 a = boundsMin;
                Vector3 b = new Vector3(boundsMin.x, boundsMin.y, boundsMax.z);
                Vector3 c = new Vector3(boundsMax.x, boundsMin.y, boundsMax.z);
                Vector3 d = new Vector3(boundsMax.x, boundsMin.y, boundsMin.z);

                DrawCorner(a, cornerX, cornerY, cornerZ);
                a.y = maxY;
                DrawCorner(a, cornerX, -cornerY, cornerZ);

                DrawCorner(b, cornerX, cornerY, -cornerZ);
                b.y = maxY;
                DrawCorner(b, cornerX, -cornerY, -cornerZ);

                DrawCorner(c, -cornerX, cornerY, -cornerZ);
                c.y = maxY;
                DrawCorner(c, -cornerX, -cornerY, -cornerZ);

                DrawCorner(d, -cornerX, cornerY, cornerZ);
                d.y = maxY;
                DrawCorner(d, -cornerX, -cornerY, cornerZ);

            }

            private static Vector3[] v3Buffer_6 = new Vector3[6];

            private static void DrawCorner(Vector3 pos, float xsize, float ysize, float zsize)
            {


                var x = new Vector3(pos.x + xsize, pos.y, pos.z);
                var y = new Vector3(pos.x, pos.y + ysize, pos.z);
                var z = new Vector3(pos.x, pos.y, pos.z + zsize);

                v3Buffer_6[0] = pos;
                v3Buffer_6[1] = x;
                v3Buffer_6[2] = pos;
                v3Buffer_6[3] = y;
                v3Buffer_6[4] = pos;
                v3Buffer_6[5] = z;

                Handles.DrawLines(v3Buffer_6);

            }
        }



    }
#endif
}