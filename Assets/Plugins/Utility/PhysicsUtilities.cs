﻿namespace Game.Utility
{
    using UnityEngine;

    public static class PhysicsUtilities
    {
        private static Collider[] buffer = new Collider[128];

        private static SphereCollider sphere;
        private static Transform sphere_tr;
        private static CapsuleCollider capsule;
        private static Transform capsule_tr;

        static PhysicsUtilities()
        {

        }

        public static void DepenetrateSphere(SphereCollider col, LayerMask layerMask)
        {
            Transform tr = col.transform;
            int count = Physics.OverlapSphereNonAlloc(tr.position, col.radius, buffer, layerMask, QueryTriggerInteraction.Ignore);

            for (int i = 0; i < count; i++)
            {
                Collider collider = buffer[i];
                Physics.ComputePenetration(col, tr.position, tr.rotation, collider, collider.transform.position, collider.transform.rotation, out Vector3 direction, out float distance);

                tr.position += direction * distance;
            }
        }
    }

}