﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReadOnlyList<T> : IReadOnlyList<T>, IEnumerable<T>, IEnumerable, IReadOnlyCollection<T>
{
	private readonly List<T> _list;

	public T this[int index]
	{
		get
		{
			return _list[index];
		}
	}

	public int Count => _list.Count;

	public ReadOnlyList(List<T> list)
	{
		_list = list;
	}

	public IEnumerator<T> GetEnumerator()
	{
		return _list.GetEnumerator();
	}

	IEnumerator IEnumerable.GetEnumerator()
	{
		return _list.GetEnumerator();
	}
}
