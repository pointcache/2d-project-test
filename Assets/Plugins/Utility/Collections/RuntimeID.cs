﻿using System.Collections.Generic;

public class RuntimeID
{
    private const int INIT_CAPACITY = 128;
    private int _freeCount = 0;
    private int _totalCount = 0;

    private Stack<int> freeIDs = new Stack<int>(INIT_CAPACITY);

    public int GetID()
    {
        int id = 0;
        if (_freeCount > 0)
        {
            id = freeIDs.Pop();
            --_freeCount;
        }
        else
        {
            id = _totalCount++;
        }

        return id;
    }

    public void ReleaseID(int id)
    {
        ++_freeCount;
        freeIDs.Push(id);
    }
}