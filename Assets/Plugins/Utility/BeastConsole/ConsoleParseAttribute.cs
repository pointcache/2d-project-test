﻿namespace BeastConsole {
    using UnityEngine;
    using System;
    using System.Collections.Generic;

    [System.AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public class ConsoleParseAttribute : Attribute {
    }

    [System.AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Method, Inherited = false, AllowMultiple = false)]
    public class ConsoleExcludeAttribute : Attribute {
    }

    [System.AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public class ConsoleParseRvarsAttribute : Attribute {

        public string RootName { get; private set; }

        public ConsoleParseRvarsAttribute(string rootname = "")
        {
            this.RootName = rootname;   
        }
    }
}