﻿namespace BeastConsole.Backend.Internal
{

    using System;
    using System.Reflection;
    using UnityEngine;

    internal class FieldCommand : Command
    {

        internal FieldInfo m_fieldInfo;
        internal Type m_fieldType;
        internal Type m_declaringType;

        //If we detect that the field is an RVar 
        private bool isRVar;
        private bool isStatic;

        public FieldCommand(string name, string description, ConsoleBackend backend) : base(name, description, backend)
        {
        }

        internal void Initialize(FieldInfo info)
        {

            this.m_fieldInfo = info;
            if (this.m_fieldInfo.FieldType.BaseType.Name.Contains("rvar"))
            {
                this.isRVar = true;
                this.m_fieldType = this.m_fieldInfo.FieldType.GetGenericArguments()[0];
            }
            else
            {
                this.m_fieldType = this.m_fieldInfo.FieldType;
            }
            this.m_declaringType = this.m_fieldInfo.DeclaringType;
            this.isStatic = this.m_fieldInfo.IsStatic;
        }

        internal override void Execute(string line)
        {

            var split = line.TrimEnd().Split(' ');

            if (split.Length <= 1)
            {
                if (this.isStatic)
                {
                    if (this.isRVar)
                    {
                        object rvar = this.m_fieldInfo.GetValue(null);
                        PropertyInfo getter = rvar.GetType().GetProperty("Value");
                        PrintValue(getter.GetValue(rvar).ToString());
                        return;
                    }
                }

                return;
            }

            if (this.isStatic)
            {
                if (this.isRVar)
                {
                    object param = StringToObject(split[1], this.m_fieldType);
                    object rvar = this.m_fieldInfo.GetValue(null);
                    PropertyInfo setter = rvar.GetType().GetProperty("Value");
                    setter.SetValue(rvar, param, null);
                    PrintValue(this.m_fieldInfo.GetValue(null).ToString());
                }
            }
            else
            {
                var gos = GameObject.FindObjectsOfType(this.m_declaringType);
                int count = gos.Length;
                object param = StringToObject(split[1], this.m_fieldType);

                for (int i = 0; i < count; i++)
                {
                    if (this.isRVar)
                    {
                        object rvar = this.m_fieldInfo.GetValue(gos[i]);
                        PropertyInfo setter = rvar.GetType().GetProperty("Value");
                        setter.SetValue(rvar, param, null);
                        PrintValue(setter.GetValue(rvar).ToString());
                    }
                    else
                    {
                        this.m_fieldInfo.SetValue(gos[i], param);
                        PrintValue(this.m_fieldInfo.GetValue(gos[i]).ToString());
                    }
                }
            }
        }

        private void PrintValue(string value)
        {
            BeastConsole.Console.WriteLine($"[VAR]: {this.m_name} = {value}");
        }
    }
}
