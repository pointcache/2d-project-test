﻿#if UNITY_EDITOR
namespace Game.Utility
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEditor;
    using UnityEngine;

    public static class EditorUtilities
    {
        public static class IconGetter
        {
            private delegate Texture2D del(UnityEngine.Object obj);
            private static del getter;

            static IconGetter()
            {
                var method = typeof(EditorGUIUtility).GetMethod("GetIconForObject", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);

                getter = Delegate.CreateDelegate(typeof(del), method) as del;
            }

            public static Texture2D GetIcon(UnityEngine.Object obj)
            {
                return getter(obj);
            }
        }

        public static T FindScriptable<T>() where T : ScriptableObject
        {
            Type t = typeof(T);

            var assets = AssetDatabase.FindAssets($"t:{t.Name}");

            if(assets.Length > 0)
            {
                return AssetDatabase.LoadAssetAtPath<T>(AssetDatabase.GUIDToAssetPath(assets[0]));
            }

            return null;
        }
    } 
}
#endif