namespace Game.Utility.ScriptableObjectFactory {
    using System;
    using System.Linq;
    using UnityEditor;
    using UnityEditor.ProjectWindowCallback;
    using UnityEngine;

    internal class EndNameEdit : EndNameEditAction {
        #region implemented abstract members of EndNameEditAction
        public override void Action(int instanceId, string pathName, string resourceFile) {
            AssetDatabase.CreateAsset(EditorUtility.InstanceIDToObject(instanceId), AssetDatabase.GenerateUniqueAssetPath(pathName));
        }

        #endregion
    }

    /// <summary>
    /// Scriptable object window.
    /// </summary>
    public class ScriptableObjectWindow : EditorWindow {

        private class TypeData
        {
            public string Name;
            public int selectedIndex;
            public string[] names;
            public Type[] types;

            public TypeData(string name)
            {
                this.Name = name;
            }
        }

        private TypeData user = new TypeData(nameof(user));
        private TypeData editorInternal = new TypeData(nameof(editorInternal));
        private TypeData plugins = new TypeData(nameof(plugins));

        private TypeData[] datas;

        public Type[] TypesUser
        {
            get { return user.types; }
            set {
                user.types = value;
                user.names = user.types.Select(t => t.FullName).ToArray();
            }
        }

        public Type[] TypesEditorInternal
        {
            get { return editorInternal.types; }
            set {
                editorInternal.types = value;
                editorInternal.names = editorInternal.types.Select(t => t.FullName).ToArray();
            }
        }

        public Type[] TypesPlugins
        {
            get { return plugins.types; }
            set {
                plugins.types = value;
                plugins.names = plugins.types.Select(t => t.FullName).ToArray();
            }
        }

        private void OnEnable()
        {
            datas = new TypeData[]
            {
                user,
                editorInternal,
                plugins
            };
        }

        public void OnGUI() {

            foreach (var data in datas)
            {
                DrawSelector(data);
            }
        }

        private void DrawSelector(TypeData typedata)
        {
            GUILayout.Label(typedata.Name);
            typedata.selectedIndex = EditorGUILayout.Popup(typedata.selectedIndex, typedata.names);

            if (GUILayout.Button("Create")) {
                var asset = ScriptableObject.CreateInstance(typedata.types[typedata.selectedIndex]);
                ProjectWindowUtil.StartNameEditingIfProjectWindowExists(
                    asset.GetInstanceID(),
                    ScriptableObject.CreateInstance<EndNameEdit>(),
                    string.Format("{0}.asset", typedata.names[typedata.selectedIndex]),
                    AssetPreview.GetMiniThumbnail(asset),
                    null);

                Close();
            }
        }
    } 
}