﻿using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Events;

public static class UIUtilities {

   // public class ToggleMapper<T> where T :class
   // {
   //     private Dictionary<int, T> map = new Dictionary<int, T>();
   //
   //     public void AddToggle(Toggle toggle, 
   // }


    public static void AddButtonAction(this UnityEvent ev, System.Action action) {
        ev.AddListener(() => { action(); });
    }

    public static void SetCanvasGroupInteractions(this GameObject go, bool state) {
        var cg = go.GetComponent<CanvasGroup>();
        cg.interactable = state;
        cg.blocksRaycasts = state;
    }

    public static void SetCanvasGroupInteractions(this CanvasGroup cg, bool state) {
        cg.interactable = state;
        cg.blocksRaycasts = state;
    }


    public static void SetText(this GameObject go, object text) {
        go.GetComponentInChildren<Text>().text = text.ToString();
    }
    public static void SetText(this Transform go, object text) {
        go.GetComponentInChildren<Text>().text = text.ToString();
    }

    static Slider.SliderEvent emptySliderEvent = new Slider.SliderEvent();
    public static void SetValue(this Slider instance, float value)
    {
        var originalEvent = instance.onValueChanged;
        instance.onValueChanged = emptySliderEvent;
        instance.value = value;
        instance.onValueChanged = originalEvent;
    }
 
    static Toggle.ToggleEvent emptyToggleEvent = new Toggle.ToggleEvent();
    public static void SetValue(this Toggle instance, bool value)
    {
        var originalEvent = instance.onValueChanged;
        instance.onValueChanged = emptyToggleEvent;
        instance.isOn = value;
        instance.onValueChanged = originalEvent;
    }
 
    static InputField.OnChangeEvent emptyInputFieldEvent = new InputField.OnChangeEvent();
    public static void SetValue(this InputField instance, string value)
    {
        var originalEvent = instance.onValueChanged;
        instance.onValueChanged = emptyInputFieldEvent;
        instance.text = value;
        instance.onValueChanged = originalEvent;
    }

    public static void SetAplha(this Image image, float value)
    {
        Color c = image.color;
        c.a = value;
        image.color = c;
    }

 
    // TODO: Add more UI types here.
}
